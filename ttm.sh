#!/bin/bash
# Simple file uploader to ttm.sh
if [ -z $1 ]; then
	echo "usage: FILENAME"
else 	
	cat $1 | curl -F file=@- https://ttm.sh
fi	
