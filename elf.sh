#!/bin/sh

# Simple script to add ELF header before hex instructions

CODE=$1
BINFILE=$2
ELF_HEADER=$3

cat $ELF_HEADER > $BINFILE
echo -n -e '$CODE' >> $BINFILE
